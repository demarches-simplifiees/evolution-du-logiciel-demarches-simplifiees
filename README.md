# Évolution du logiciel Démarches-Simplifiées (DS)

L’ADULLACT mutualise, étudie et relaye les
[demandes d'**évolution** du **logiciel DS**](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues)
auprès de la _DINUM_ pour les **collectivités** et les **ESR** (_établissements d’enseignement supérieur ou de recherche_).
Ce projet permet un travail collaboratif entre les collectivités, les ESR et l’Adullact
pour discuter des évolutions du logiciel DS. Ce projet s’organise avec ce fichier `README`
comme documentation et les demandes d’évolution sous forme de ticket Gitlab.

- [Vocabulaire](#vocabulaire)
- [Principes](#principes)
- [Demandes d’évolution déjà proposées](#demandes-d%C3%A9volution-d%C3%A9j%C3%A0-propos%C3%A9es)
- [Proposer une fonctionnalité](#comment-proposer-une-nouvelle-fonctionnalit%C3%A9-ou-une-%C3%A9volution-dune-fonctionnalit%C3%A9-existante-)
- [Vocabulaire métier](#vocabulaire-métier)


---------------------

## Vocabulaire

- **collectivités** = ville, département, région, EPCI, ...
- **ESR** = universités et établissements d'enseignement supérieur ou de recherche

---------------------

- **DS** = Logiciel "Démarches Simplifiées"
- **DS.fr** = Instance DS de la DINUM = service [demarches-simplifiees.fr](https://demarches-simplifiees.fr) utilisant le logiciel DS
- **Instance DS** de l'**Adullact** = service [demarches.adullact.org](https://demarches.adullact.org) utilisant le logiciel DS
- **Instance DS** = service en ligne utilisant le logiciel DS
- L'**équipe DS** = l’équipe de la DINUM en charge du développement du logiciel DS et la gestion de l’instance DS de l’état (DS.fr).

Voir aussi le [vocabulaire métier](#vocabulaire-métier)


---------------------

## Demandes d’évolution déjà proposées

Consulter l’ensemble des [demandes d’évolution déjà proposées](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues)
<br> ou consulter ces demandes par type d’utilisateur :

- [fonctionnalités pour les **super-admins**](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues/?label_name%5B%5D=ETQ%20super-admin)
- [fonctionnalités pour les **administrateurs**](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues/?label_name%5B%5D=ETQ%20administrateur)
- [fonctionnalités pour les **instructeurs**](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues/?label_name%5B%5D=ETQ%20instructeur)
- [fonctionnalités pour les **experts**](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues/?label_name%5B%5D=ETQ%20expert)
- [fonctionnalités pour les **invités**](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues/?label_name%5B%5D=ETQ%20invit%C3%A9)
- [fonctionnalités pour les **usagers**](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues/?label_name%5B%5D=ETQ%20usager)


---------------------

## Principes

- **No fork** : les fonctionnalités doivent être développées pour être intégrées
  dans le code du logiciel DS et une phase d’échange avec l’équipe DS sera nécessaire en amont du code.
- Le **temps** de l'**équipe DS** est **précieux** et l’objectif est de ne pas surcharger l’équipe DS qui a sa propre roadmap.
- L’ADULLACT  **mutualise** et relaye les **demandes d’évolution** pour les **collectivités** auprès de la DINUM.
- L’AMUE et l’ADULLACT  mutualisent et relayent les demandes d’évolution pour les ESR.


---------------------

## Comment proposer une nouvelle fonctionnalité ou une évolution d’une fonctionnalité existante ?

Pour les demandes d’évolution du logiciel DS, comme une **nouvelle fonctionnalité** ou l’évolution d’une fonctionnalité existante, nous vous invitons à suivre processus suivant :

1. Vérifier si le besoin est dans les [demandes d’évolution déjà proposées](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues).
   - Si ce besoin **est déjà** présent, vous pouvez participer en ajoutant un commentaire directement sur le ticket.
   - Si ce besoin **n’est pas** présent, vous pouvez passer à l’étape suivante.
1. [Créer un compte sur le GitLab ADULLACT](https://faq.adullact.org/technique/forge/gitlab/premiers-pas-sur-gitlab-adullact/#se-cr%C3%A9er-un-compte-sur-le-gitlab-adullact), si vous n’avez pas de compte.
1. [Créer un **nouveau ticket**](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues/new)
   dans ce dépôt pour décrire le besoin. (exemple : [ticket #8](https://gitlab.adullact.net/demarches-simplifiees/evolution-du-logiciel-demarches-simplifiees/-/issues/8))
   - contexte et résumé du besoin
   - rédiger les "user stories" (les besoins du point de vue des utilisateurs)
   - rédiger les "tests d’acceptation" (le descriptif des fonctionnements attendus)

Les **échanges** autour de ce besoin se poursuivront **directement** dans **ce nouveau ticket** avec les autres collectivités ou ESR, l’_Adullact_, l’_AMUE_ et la _DINUM_.

À chaque nouveau ticket, l’Adullact vérifiera les points suivants :

- si c’est une fonctionnalité existante et activée sur l’instance de l’Adullact ?
- si c’est une fonctionnalité en cours de développement ?
- si c’est une fonctionnalité intéressante, mais non planifiée par l’équipe DS ?
- s’il faut affiner l’analyse du besoin ?
- s’il existe des solutions alternatives ?
- …


---------------------

## Vocabulaire métier

Vocabulaire métier à utiliser dans vos tickets (bug, nouvelle fonctionnalité et demande d’évolution)

- **Démarche** : démarche administrative créée par une collectivité,
  à destination de ses usagers (personne ou organisation). Exemple : inscription à la cantine, demande d’état civil…
- **Dossier** : ensemble des informations saisies par un usager (personne ou organisation) pour une démarche.
- **Usager** : personne ou organisation qui dépose un dossier sur une démarche (l’inscription se fait de manière autonome).
- **Invité** : personne sollicitée par un usager pour modifier son dossier.
- **Expert** : personne sollicitée par un instructeur pour donner son avis sur un dossier.
- **Instructeur** : personne chargée de traiter les dossiers d’une démarche.
- **Administrateur** : personne chargée de la création des démarches et de la gestion des instructeurs sur les démarches (ajout et suppression).
- **Super-Admin** : personne chargée de la gestion des administrateurs et d’autres tâches de gestion de l’instance DS.

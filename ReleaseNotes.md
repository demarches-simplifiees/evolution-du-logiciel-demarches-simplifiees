# Release Notes des évolutions et correctifs déployés sur demarches.adullact.org

Issu des informations du dépôt [Github demarches-simplifiees](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases)

## V11.0.0

le 19 aout 2024

### Nouveautés

#### Administrateur

- [SVA/SVR] - mise à disposition d'une tuile pour activer le SVA / SVR (Silence vaut acceptation ou refus) par démarche
- [Accusé de lecture] - possibilité de configurer une démarche avec accusé de lecture [#10190](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10190)
accusé de lecture (pour les démarches avec voies de recours) L‘activation se fait depuis la tuile Accusé de lecture (en bas de la page d’accueil de la démarche).
- [critères d'éligibilité] - possibilité d'ajouter des conditions d'éligibilité auxquelles les dossiers doivent correspondre sans quoi l'usager ne peut déposer son dossier [#10292](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10292)
- [Routage instructeur] - Ajouter des instructeurs ou inviter des experts peut se faire en masse en copiant/collant des listes d‘adresse emails qui doivent être séparées par des points-virgules.
- [Cloture d'une démarche] - L’administrateur peut désormais écrire un texte expliquant les raisons de la fermeture et le publier sur une page dédiée. Les usagers arriveront sur cette page s’ils se connectent via le lien public de la démarche.
L’administrateur peut aussi envoyer un email à tous les usagers avec un dossier en brouillon ou avec un dossier déposé.
- [Cloture d'une démarche] - possibilité d'alerter les usagers et de créer une page de fermeture si la démarche n'est pas redirigée dans DS [#9930](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9930)

#### Instructeur et Usagers

- mise en place d'une galerie afin de faciliter la vue des pièces jointes d'un dossier [#10281](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10281)
- possibilité d'ajouter plusieurs Pièces Jointes d'un seul coup dans la messagerie [#9986](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9986)

#### Expert

- [Notifications] activation de la gestion des notifications reçues des experts pour chaque démarche ou l'expert est sollicité [#10127](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10127)

#### Autres

- pour les instances : necéssité de mettre en place un serveur Redis pour le bon fonctionnement des jobs (sidekiq remplace job delayed / indexation / antivirus)

### correctifs et améliorations

#### Administrateur

- amélioration de la configuration des experts invités [#10463](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10463)
- pouvoir modifier le message de confirmation de dépôt de dossier [#10522](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10522)

#### Instructeur

- les images sont tournées dans le bon sens [#10364](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10364) et [#10440](ttps://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10440)
- ordonnancement des avis du plus recent au plus ancien [#10493](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10493)
- le nombre de dossier par page passe de 25 a 100 [#10535](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10535)

#### Usager

- je souhaite pouvoir visualiser un formulaire au format PDF [#10092](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10092)
- Champ choix multiple, ajoute une aide à la saisie [#10220](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10220)
- le champ carte gagne en accessibilité [#10314](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10314)
- légère amélioration du message à propos d'une démarche close [#10402](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10402)
- pouvoir télécharger son dossier sur la page de confirmation de dépôt [#10497](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10497)
- télécharger son dossier sans avoir à passer par le bouton "imprimer" [#10559](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10559)
- Corrige l'affichage du champ "choix simple" ayant plus de 20 options [#10592](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10592)

#### invité

- afficher le propriétaire du dossier dans la liste des dossiers [#10133](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10133)

#### Autres

- ne pas voir apparaitre des balises HTML dans mon attestation au format pdf [#10081](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10081)

## V10.0.0

Le 18 avril 2024

### nouveautés / évolutions

- [accessibilité] Mise en place du thème sombre
- [accessibilité] ajout d'un pied de page pour les administrateurs et les instructeurs [#9817](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9817)

#### gestionnaire

- [délégation de création d'administrateurs] déploiement de la fonctionnalité [#9571](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9571)

#### administrateur

- possibilité de conditionner les annotations en utilisant des champs du formulaire [#9660](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9660)
- [routage instructeur] création d'une règle de routage de plusieurs lignes [#9604](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9604)
- [routage instructeur] je peux gérer la liste des instructeurs même si le routage est activé sur ma démarche [#10000](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10000)
- je peux conditionner / router avec un champ de type nombre décimal [#9694](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9694)
- Chorus: je peux ajouter un type de champ EngagementJuridique [#9621](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9621)
- je peux conditionner / router à partir d'un champ de type choix multiple avec l'opérateur "Ne contient pas" [#9714](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9714)
- [éditeur de formulaire] je peux ajouter une notice explicative (PJ) a un champ explication [#9871](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9871)
- [éditeur de formulaire] dans l'éditeur de formulaire je peux déplacer un champ par une liste [#9861](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9861)
- [éditeur de formulaire] dans l'éditeur de formulaire un sommaire permet de naviguer de section en section [#9894](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9894)
- [éditeur de formulaire] déplacer un champ dans l'éditeur est plus rapide [#9919](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9919), [#9922](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9922), [#9927](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/99927)
- [éditeur de formulaire] je peux ajouter un champ carte dans un bloc répétable [#9907](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9907), [#9979](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9907)
- je peux copier une liste d'emails pour inviter des experts [#9966](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9966)

#### instructeur

- lorsque je motive une décision, la motivation est désormais bien formatée [#9799](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9799) [#10050](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10050)
- je peux accéder à la page des dossiers supprimés [#9814](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9814)
- j'ai plus d'option pour le traitement multiple : supprimer, restaurer, desarchiver, passer en instruction depuis l'onglet "suivre", [#9772](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9772)
- je peux copier une liste d'emails pour inviter des experts [#9966](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9966)

#### usager

- [UX] refonte de la page usager
- ETQ usager je peux éditer jusqu'à 50 points ou éléments sélectionnés dans le champ carte [#9989](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9989)
- [routage instructeur] je vois les informations de contact de mon groupe instructeur (si elles existent) dans mon attestation de dépôt [#10067](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/10067)

### corrections

#### expert

- je suis notifié des nouveaux messages [#9890](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9890)

## V9.0.0

Le 19 décembre 2023

### nouveautés et évolutions

#### super admin

- j’ai la possibilité de masquer une démarche de la liste des démarches disponibles à la création [#9345](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9345)
- Je peux visualiser de la liste des instructeurs et des groupes d'instructeurs pour une procédure spécifique [#9448](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9448)
- je peux supprimer un administrateur depuis le manager [#9616](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9616)

#### administrateurs

- Mise à jour de l'UX de la page groupe d'instructeurs [#9146](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9146)
- je voudrais proposer de pré-remplir les annotations privées [#9197](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9197)
- je vois une modale pour confirmer la réinitialisation des modifications de la procédure [#9188](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9188)
- je voudrais pouvoir pré-remplir un dossier sur une démarche en test [#9166](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9166)
- je souhaite voir les erreurs lorsque la (re-)publication échoue [#9196](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9196)
- [Page accueil demarche] j’ai la possibilité de créer ma propre liste de PJ [#9234](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9234)
- je ne vois plus l'IBAN comme un champ "suspect" [#9208](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9208)
- je peux importer des instructeurs depuis la page d'un groupe [#9168](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9168)
- la page "publier" est plus claire [#9323](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9323)
- je suis alerté lorsque mes demarches publiées ont des services sans siret ou pas de service [#9353](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9353)
- [Conditionnel] je peux conditionner sur un champ "département" [#9506](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9506)
- [Conditionnel] je peux conditionner sur les champs communes, EPCI et région [#9614](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9614)
- [Conditionnel] je peux conditionner  dans un champ d'un bloc répétable [#9556](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9556)
- [Conditionnel] active le conditionnel dans les annotations privées [#9546](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9546)
- [Routage] je suis alerté à la publication d’une démarche si des groupes n'ont pas de règle de routage valide [#9388](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9388)
- [Routage] je peux filtrer les groupes d'instructeurs à configurer [#9164](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9164)
- [Routage] je peux router par département depuis un type de champ communes [#9474](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9474)
- [Routage] je peux router par département depuis le champ EPCI [#9477](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9477)
- [Routage] je peux router depuis un champ de type "régions" [#9472](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9472)
- [Routage] je peux router depuis un champ département [#9406](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9'à-)
- [Routage] je peux router avec des règles dont l'opérateur est "n'est pas" [#9423](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9423)
- Intégration de la logique conditionnelle pour la cartographie dans les annotations privées

#### instructeurs

- je souhaite que les filtres fassent la différence entre les Bouches-du-Rhône et le Rhône [#9219](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9219)
- je peux réaffecter un dossier à un autre groupe d'instructeurs [#9093](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9093)
- intègre les demandes de corrections dans les PDF [#9260](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9260)
- [Actions multiples] Ajouter la possibilité pour les instructeurs de classer sans suite et refuser [#9274](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9274)
- les démarches sont filtrées par onglet pour les retrouver plus facilement [#9308](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9308)
- je vois les réaffectations d'un dossier [#9270](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9270)
- je peux envoyer un message a un utilisateur ayant un dossier qui n'a pas encore de groupe d'instructeur (statut en brouillon) [#9314](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9314)
- je peux filtrer les dossiers "en construction" sans inclure ceux "en attente de corrections" [#9520](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9520)
- je peux ajouter une fonction d'auto-complétion lors de la demande d'avis aux experts [#9471](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9471)
- nouveau filigrane pour les titres d'identité améliorant la lisibilité des documents Titres d'identité [#9470](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9470)
- je vois un badge d'alerte quand le dossier a été réaffecté [#9286](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9286)
- Création d'une page qui liste les exports ; retrait des liens des menus déroulants [#9473](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9473)
- réorganisation des onglets et mise en avant des démarches closes avec des dossiers à traiter [#9517](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9517), [#9543](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9543)
- je suis instructeur ou administrateur, j’ai la possibilité d'apposer un tampon dédié à un groupe instructeur sur une attestation [#9507](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9507)
- Le PDF d'un dossier intègre les questions et messages des avis experts [#9555](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9555)
- Ajoute les pièces justificatives des avis dans les exports [#9558](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9558)
- [design] améliorations visuelles sur la page récap d'un dossier [#9249](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9259)
- Possibilité de masquer /démasquer les différents titres de sections (uniquement sur le 1er niveau)
- modification de l'emplacement et de la forme du bouton de 'Personnalisation'du tableau bord d'une démarche
- Evolution de la page "statistiques" intégrant un temps de traitement usuel

#### utilisateurs

- [design] Changement de couleurs pour les bagdes de statuts des dossiers [#9156](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9156)
- [design] la page résumant ma demande est aux couleurs du DSFR (design system de l’État Français) [#9030](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9030)
- [design] améliorations visuelles sur la page récap d'un dossier [#9249](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9249))
- je suis guidé dans le formulaire lorsque les champs sont obligatoires ou facultatifs [#9050](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9050)
- [Accessibilité] le taux de conformité passe à 80% [#9192](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9192)
- je ne reçois pas d'email si j'ai supprimé mon compte [#9221](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9221)
- je peux corriger l'identité incomplète d’un dossier après dépôt [#9288](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9288)
- je bénéficie d'une aide à la saisie grâce à une description détaillée du format attendu pour certains champs [#9518](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9518)
- je peux voir dans mon dossier les informations de contact de mon groupe instructeur [#9425](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9425)
- je souhaite toujours avoir une liste déroulante sans choix présélectionné [#8991](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8991)
- la saisie d'un nombre invalide affiche une erreur [#9516](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9516)
- disparition de la mention "facultatif" sur les champs optionnels [#9553](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9553)

#### Invité

- je visualise plus facilement l’information qui stipule que c’est bien au titulaire du dossier de déposer celui-ci [#9322](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9322)

#### technique

- je souhaite mettre à jour les données de l'API GEO [#9241](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9241)
- [Rails] mise à jour de rails v.7.0.4.3 => v.7.0.5.1 [#9253](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9253)
- amelioration(email_event): re-lever une erreur dans un rescue_from ne la fait pas remonter. change de stratégie pour savoir si oui ou non un mail a été envoyé avec success. [#9209](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9209)
- je peux bloquer un compte [#9324](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9324)
- je peux piloter le niveau de log à partir de la variable DS_LOG_LEVEL [#9342](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9342)
- [API Entreprise] envoie le siret par défaut si le siret du service est identique au siret de l'établissement demandé [#9365](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9365)
- [Délégation de création de compte administrateurs] Initialisation des gestionnaires de groupes [#9418](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9418)

### correctifs bugs

- Corrige le tableau de bord usager lorsqu'il y a un dossier supprimé sur une démarche en cours de suppression [#9158](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9158)
- [routage instructeur] rejoue le moteur de routage après ajout des règles de routage sur une procédure clonée [#9194](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9194)
- correctif(dossier.en_construction): un usager, peut modifier un dossier ayant des repetition ayant des enfants [#9268](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9268)
- En tant qu'usager ou instructeur, je ne souhaite pas voir les champs conditionné et non visible dans un bloc repetable [#9267](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9267)
- un usager peut saisir des nombres négatifs [#9570](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9570)
- [champs.iban] en tant qu'usager, le copier/coller sur un champs iban avec le code pays en minuscule perdait les caractères alphabétiques en debut d'IBAN [#9321](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9321)
- Correctif : un instructeur à la possibilité de remonter les dossiers avec une notification

-------------------------

## V8.0.0

Le 20 juillet 2023

### évolutions

#### Administrateurs

- je peux filtrer avec plusieurs tags [#8720](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8720)
- permet l'import CSV de groupes instructeur avec routage lorsque la démarche n'a pas encore de routage actif [#8733](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8733)
- je peux bloquer l'accès aux experts à la messagerie [#8805](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8805)
- conserve les filtres dans de la pagination de "toutes les démarches [#8815](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8815)
- je peux changer le groupe d'instructeur par défaut [#8916](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8916)
- je peux specifier un niveau de titre a mes titres de sections [#8695](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8695)
- je veux être averti si des données seront supprimées sur les dossiers lors de la publication d’une nouvelle révision [#8963](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8963)
- je peux prévisualiser la démarche depuis la page "toutes les démarches" [#8995](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8995)
- je veux pouvoir limiter le nombre de caractères dans les champs "texte long" [#8990](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8990)
- je veux avoir une première démarche pré remplie lorsque j'arrive sur ds [#8997](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8997)
- lors de la création ou modification d'une démarche, des zones par défaut me sont suggérées [#9014](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9014)
- Filtrer les démarches par service [#8887](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8887)

#### Instructeurs

- active la sauvegarde automatique des annotations privées [#8727](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8727)
- integre les avis dans l'export au format pdf du dossier [#8744](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8744)
- Je peux poser une question dans une demande d'avis [#8743](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8743), [#8789](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8789), [#8816](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8816)
- Ajoute un raccourci pour copier un SIRET [#8781](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8781)
- je veux pouvoir supprimer la pj pendant l'instruction [#8811](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8811)
- je veux voir l'état des dossiers affiché sur une seule ligne [#8920](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8920)
- je veux pouvoir accéder à mes annotations privées [#8955](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8955)
- je peux relancer une demande d'avis meme si celui ci contient une question [#8966](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8966)
- je peux marquer un dossier "à corriger" par l'usager [#8714](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8714), [#9134](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9134)
- je peux filtrer les dossiers par avis [#9076](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9076)

#### Utilisateurs

- peut révoquer une demande de transfert [#8728](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8728)
- je voudrai pouvoir remplir un code postal avec des espaces [#8930](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8930)
- Tableau de bord - Déplacer la barre de recherche proche des dossiers [#8912](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8912)
- je voudrais avoir accès aux statistiques des démarches closes [#8928](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8928)
- je peux créer de nouveaux dossiers sur la démarche qui remplace la démarche fermée [#8943](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8943)
- je veux être avertie si l'API entreprise est HS [#8984](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8984)
- je voudrais pouvoir déposer les changements apportés à la carte dans un dossier en construction [#9140](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9140)
- regroupe les champs précédés d'un titre de section dans un fieldset [#8695](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8695)
- Simplifie l'implémentation des champs "liste d'options" et "listes d'options liées [#8850](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8850)

##### Technique

- Api Entreprise V3 remplace la V2

### Corrections et améliorations

#### Usager

- Corrige l'alignement des badges du dossier côté usager
- Améliore l'affichage des resultats de recherche
- Désactive la checkbox pour tout séléctionner quand une action mutliple est en cours
- correction sur les titres de section conditionnés
- je veux que les champs de type adresse électronique soit validé [#8899](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8899)
- je veux que mon interface soit traduite en anglais [#8921](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8921)
- utilisant microsoft Edge, les champs de type date sont mal alignés [#8962](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8962)
- je trouve que les titres de section sont trop fort en graisse de caractère [#8952](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/8952)
- je ne veux pas bypasser la vérification de SIRET
- je ne veux pas de caractère invalide dans l'état du dossier en PDF
- je modifie et soumets à nouveau mon dossier “en construction", et je peux supprimer une PJ obligatoire
- je souhaite pouvoir acceder aux champs en erreur facilement
- améliore la visibilité du bouton action dans le tableau dossiers des usagers
- le champ PJ d'un dossier en construction ne doit pas être marqué "à déposer" sans raison
- brouillon : je souhaite que chaque lien pointant vers une erreur de champ m'oriente sur le champ
- je peux modifier un dossier en_construction qui contient des champs en erreur [#9086](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9086)
- débloque l'accès à des dossiers à cause d'incohérences entre les champs et les révisions associées [#9077](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9077)
- modification de wording du footer dans le cadre d'une démarche [#9087](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9087)
- je peux joindre une pièce justificative audio en .m4a, .aac, .wav
- Tableau de bord - Améliorer pagination
- Tableau de bord - Ajouter continuer à remplir dernier dossier

- usager invité sur un dossier, je peux ajouter une PJ d'un dossier en construction [#9088](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9088)

#### Administrateur

- empêche une condition d'égalité de s'applique à un champ choix multiple
- je veux cloner la version publiée des démarches en production

#### Instructeur

- Ne pas notifier le groupe quand un instructeur est enlevé de celui-ci
- amélioration UX de la demande d'avis
- Uniformise les actions pour les instructeurs sur la page tableau et dossier
- peut supprimer un admin sans démarche publiée
- Lorsque je saisi une autre valeur dans un champ de type choix simple, celle ci n'était pas sauvegardée
- je ne veux pas d'erreurs quand les filtres ne sont pas trouvés
- ne plante pas un export s'il contient des PJ avec des noms de fichiers invalides
- je souhaite avoir les mêmes compteurs entre les pages listant mes demarches et la page pour visualiser une démarches

#### Expert

- lorsque j'exporte un dossier au format PDF, celui ci contenient les avis non confidentiels ainsi que mes avis
- je veux retrouver mes avis donnés sur des dossiers traités

#### Dossier

- ne numérote pas automatiquement les titres de section dans les répétitions
- ne crash pas lorsqu'un champ date n'as pas une date standard
- corrige la numérotation automatique des annotations privées
- correction du crash d'annotations privées à cause d'une incohérence avec leur type de champ
- les annotations privées n'étaient pas correctement rebasées après modif de leur formulaire
- accessibilité : augmente le contraste de la barre de progression lorsqu'elle a le focus

#### Formulaire

- ajoute le formulaire pour supprimer une PJ
- désactive le changement au scroll
- correction de certaines parcelles cadastrales ne contiennent pas la surface
- le "détail" du champ explication peut être formatté avec des liens

## V7.0.O

le 19 avril 2024

## V6.0.0

le 19 janvier 2023

## V5.0.O

le 24 mai 2022

## V4.0.O

le 27 avril 2022

## V3.0.O

le 11 mars 2022

## V2.0.O

le 31 janvier 2022

## V1.0.O

First release en février 2021

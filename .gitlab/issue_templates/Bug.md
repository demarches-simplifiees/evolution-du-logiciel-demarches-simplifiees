## Résumé

<!-- Résumez brièvement le bug -->


## Étapes pour reproduire le bug

<!-- Que devez-vous faire pour reproduire le bug ? -->


## Comportement actuel

<!-- Ce qui se passe réellement -->


## Comportement attendu

<!-- Ce que vous devriez voir à la place -->


## Logs et/ou captures d'écran pertinents

<!-- Collez les logs à l'intérieur des blocs de code (```)
     ci-dessous afin qu'ils soient plus faciles à lire.           -->

<details>
<summary> Journal </summary>

```sh
Ajoutez le journal ici
```
</details>


## Corrections possibles

<!-- Si vous le pouvez, faites un lien vers la ligne de code
     qui pourrait être responsable du problème -->



/label ~BUG

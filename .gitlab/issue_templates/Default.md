<!-- Demande d'évolution du logiciel Démarches-Simplifiées (DS)
     pour les collectivités et les ESR (universités, établissements d'enseignement supérieur ou de recherche) -->

## Status

<!-- L'édition du status est réalisé par l'Adullact -->
- [ ] Expression du besoin rédigée
- [ ] Analyse rapide par l'Adullact
- [ ] Cadré avec l'équipe DS
- [ ] Développement par ...
- [ ] Code intégré dans le logiciel DS
- [ ] Déployé sur l'instance DS de l'Adullact

## Résumé

<!-- Décrivez brièvement ci-dessous la nouvelle fonctionnalité
     ou l'évolution souhaitée d'une fonctionnalité existante. -->


## Contexte

<!-- Préciser ci-dessous  la problématique, les cas d'utilisation, les avantages et/ou les objectifs -->


## Actuellement

<!-- Décrivez ci-dessous le fonctionnement actuel de l'application DS -->


## Comportement attendu

<!-- Décrivez ci-dessous le comportement attendu de l'application DS
     avec cette nouvelle fonctionnalité ou cette évolution d'une fonctionnalité existante. -->


----------------

## Cahier des charges

<!-- Description normalisée (à faire maintenant ou plus tard) :
     - du besoin de l'utilisateur (usager, instructeur...) sous forme de "user story",
     - du fonctionnement attendu du logiciel sous forme de "tests d’acceptation".   -->

### User story

<!-- Description du besoin du point de vue de l'utilisateur (usager, instructeur, administrateur, ...)
     sous forme de "User story" pour faciliter la compréhension par tous (développeur, ergonome, décideur) -->

> Format : “En tant que” (ETQ), “je souhaite”, “afin de”

> **TODO** : à rédiger (maintenant ou plus tard)
```
ETQ instructeur,
Je souhaite, ...
Afin de ...
```


### Tests d'acceptation

<!-- Description du fonctionnement du logiciel du point de vue d'utilisateur (usager, instructeur...)
     sous forme de "tests d'acceptation" pour faciliter la compréhension par tous (développeur, ergonome...) -->

> Format : “En tant que” (ETQ), “Quand je”, “Alors”

> **TODO** : à rédiger (maintenant ou plus tard)
```gherkin
ETQ instructeur
Quand je ...
Alors ...
```
